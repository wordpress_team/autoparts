<?php /* Wrapper Name: Header */ ?>
<div class="row">
	<div class="span5" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
		<?php get_template_part("static/static-logo"); ?>
	</div>
	<div class="span7">
		<div class="header_box">
			<?php dynamic_sidebar("header-sidebar"); ?>
			<?php get_template_part("static/static-shop-nav"); ?>
		</div>
		<div class="clear"></div>
		<?php get_template_part("static/static-search"); ?>
	</div>
</div>
<div class="row">
	<div class="span12 menu_cart">
		<?php get_template_part("static/static-nav"); ?>
		<?php dynamic_sidebar( 'cart-holder' ); ?>
		<div class="clear"></div>
	</div>
</div>