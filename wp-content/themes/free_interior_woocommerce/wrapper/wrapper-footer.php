<?php /* Wrapper Name: Footer */ ?>
<div class="row">
	<div class="span12  footer-widgets">
		<div class="row">
			<div class="span4" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-1">
				<?php dynamic_sidebar("footer-sidebar-1"); ?>
			</div>
			<div class="span4" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-2">
				<?php dynamic_sidebar("footer-sidebar-2"); ?>
			</div>
			<div class="span4" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-3">
				<?php dynamic_sidebar("footer-sidebar-3"); ?>
			</div>
		</div>
	</div>
</div>
<div class="row copyright">
	<div class="span12">
		<?php get_template_part("static/static-footer-nav"); ?>
	</div>
	<div class="clear"></div>
	<div class="span4 pull-right">
		<?php dynamic_sidebar("footer-sidebar-4"); ?>
	</div>
	<div class="span8 pull-left">
		<?php get_template_part("static/static-logo2"); ?>
		<?php get_template_part("static/static-footer-text"); ?>
	</div>
</div>