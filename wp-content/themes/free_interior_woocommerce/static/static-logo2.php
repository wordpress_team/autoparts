<?php /* Static Name: Logo2 */ ?>
<!-- BEGIN LOGO -->
<div class="logo2">
	<?php if(of_get_option('logo_type') == 'text_logo'){?>
			<?php if( is_front_page() || is_home() || is_404() ) { ?>
					<h1 class="logo_h logo_h__txt"><a href="<?php echo home_url(); ?>/" title="<?php bloginfo('description'); ?>" class="logo_link"><?php bloginfo('name'); ?></a></h1>
			<?php } else { ?>
					<h2 class="logo_h logo_h__txt"><a href="<?php echo home_url(); ?>/" title="<?php bloginfo('description'); ?>" class="logo_link"><?php bloginfo('name'); ?></a></h2>
			<?php } ?>
	<?php } else { ?>
			<?php if(of_get_option('logo2_url') == ''){ ?>
					<a href="<?php echo home_url(); ?>/" class="logo_h logo_h__img"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_footer.png" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('description'); ?>"></a>
			<?php } else  { ?>
					<a href="<?php echo home_url(); ?>/" class="logo_h logo_h__img"><img src="<?php echo of_get_option('logo2_url', '' ); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('description'); ?>"></a>
			<?php }?>
	<?php }

	$tagline = get_bloginfo('description');
	if ( $tagline!='' ) { ?>
		<!-- Site Tagline -->
	<?php } ?>
</div>
<!-- END LOGO -->